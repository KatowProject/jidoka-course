# -*- coding: utf-8 -*-

from odoo import models, fields, api

class InheritPartner(models.Model):
    _inherit = 'res.partner'
    
    is_instructor = fields.Boolean('Instructor', default=False)
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
