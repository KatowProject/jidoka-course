# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class Session(models.Model):
    _name = 'jidokaacademy.session'
    _description = 'jidokaacademy.session'

    name = fields.Char(string='Title', required=True)
    start_date = fields.Date(string='Start Date', default=fields.Date.today())
    duration = fields.Float(string='Duration')
    number_of_seats = fields.Float(string='Number of Seats')
    description = fields.Text(string='Description')
    # partner_id = is_instructor
    partner_id = fields.Many2one('res.partner', string='Instructor', domain=[('is_instructor', '=', True)])
    partner_ids = fields.Many2many('res.partner', string='Attendees')
    course_id = fields.Many2one('jidokaacademy.course', string='Course', required=True)
    taken_seats = fields.Float(string='Taken Seats', compute='_count_taken_seats')
    active = fields.Boolean(string='Active', default=True)
    
    def _count_taken_seats(self):
        for session in self:
            if session.partner_ids and session.number_of_seats:
                session.taken_seats = len(session.partner_ids) / session.number_of_seats * 100
            else:
                session.taken_seats = 0
                
    @api.onchange('number_of_seats', 'partner_ids')
    def _onchange_number_of_seats(self):
        if self.number_of_seats < 0:
            return {
                'warning': {
                    'title': 'Incorrect "number of seats" value',
                    'message': 'Number of seats must be positive',
                },
            }
        if self.number_of_seats < len(self.partner_ids):
            return {
                'warning': {
                    'title': 'Too many attendees',
                    'message': 'There are more attendees than seats',
                },
            }
            
    @api.constrains('partner_ids', 'partner_id')
    def _check_attendees(self):
        for session in self:
            if session.partner_id in session.partner_ids:
                raise ValidationError(f'Instructor {session.partner_id.name} cannot be an attendee')
#           
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
