# -*- coding: utf-8 -*-
from odoo import models, fields, api

class jidokaacademy(models.Model):
    _name = 'jidokaacademy.course'
    _description = 'jidokaacademy.course'

        
    _sql_constraints = [
        ('check_name_description', 'CHECK(name != description)', 'The title of the course should not be the description'),
        ('check_name_unique', 'UNIQUE(name)', 'Another course already exists with this title'),
    ]
    
    def copy(self, default=None):
        default = dict(default or {})
        
        copied_count = self.search_count([('name', '=like', u"Copy of {}%".format(self.name))])
        
        if not copied_count:
            new_name = u"Copy of {}".format(self.name)
        else:
            new_name = u"Copy of {} ({})".format(self.name, copied_count)
        
        default['name'] = new_name
        
        return super(jidokaacademy, self).copy(default)
    
    name = fields.Char(string='Title', required=True)
    user_id = fields.Many2one('res.users', string='Responsible User')
    description = fields.Text(string='Description')
    session_ids = fields.One2many('jidokaacademy.session', 'course_id', string='Sessions')
#
#     @api.depends('value')
#     def _value_pc(self):
#         for record in self:
#             record.value2 = float(record.value) / 100
